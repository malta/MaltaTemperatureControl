#include "EnvironmentalSensor.h"

EnvironmentalSensor::EnvironmentalSensor(int tempinpin, int huminpin, double humslope, double humoffset):m_tempinpin(tempinpin), m_huminpin(huminpin),m_temp(0.), m_hum(0.), m_dew(0.){
  m_oneWire = new OneWire(m_tempinpin);
  m_tempSensor = new DallasTemperature(m_oneWire);
  m_tempSensor->begin(); 
  m_humSensor = new HumiditySensor(m_huminpin, humslope, humoffset);
}

EnvironmentalSensor::~EnvironmentalSensor(){
  delete m_humSensor;
  delete m_tempSensor;
  delete m_oneWire;
}

void EnvironmentalSensor::Read(){
  m_tempSensor->requestTemperatures();
  m_temp = m_tempSensor->getTempCByIndex(0);
  m_hum = m_humSensor->getHumidity(m_temp);
  m_dew = -99999.;
  return;
}

double EnvironmentalSensor::getTemperature(){
  return m_temp;
}

double EnvironmentalSensor::getHumidity(){
  return m_hum;
}

double EnvironmentalSensor::getDewPoint(){
  double l_hum = (m_hum < 0)?0.001:m_hum;
  if (m_dew < -9000.){
    //double l_gamma = log(l_hum/100)+b*m_temp/(c+m_temp);
    //m_dew = c*l_gamma / (b - l_gamma);

    //--------------
    //from http://irtfweb.ifa.hawaii.edu/~tcs3/tcs3/Misc/Dewpoint_Calculation_Humidity_Sensor_E.pdf
    //for -45C <= T <= 60C, error ±0.35C
    double H = (log10(l_hum)-2)/0.4343 + (17.62*m_temp)/(243.12+m_temp);
    m_dew = 243.12*H/(17.62-H);       // this is the dew point in Celsius
  }
  return m_dew;
}

