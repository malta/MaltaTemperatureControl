#ifndef EnvironmentalSensor_h
#define EnvironmentalSensor_h

#include "Arduino.h"
#include "NTC.h"
#include "HumiditySensor.h"
#include <OneWire.h>
#include <DallasTemperature.h>

class EnvironmentalSensor{
public:
  EnvironmentalSensor(int tempinpin, int huminpin, double humslope, double humoffset);
  ~EnvironmentalSensor();
  void Read();
  double getTemperature();
  double getHumidity();
  double getDewPoint();
private:
  int m_tempinpin;
  int m_huminpin;
  double m_temp, m_hum, m_dew;
  
  const double b = 18.678;
  const double c = 257.14;

  OneWire *m_oneWire;
  DallasTemperature *m_tempSensor;
  HumiditySensor *m_humSensor;
};
#endif //EnvironmentalSensor_h
