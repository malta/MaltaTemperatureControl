#include "HumiditySensor.h"

HumiditySensor::HumiditySensor(int inpin, double slope, double offset): m_inpin(inpin), m_slope(slope), m_offset(offset){}

double HumiditySensor::getHumidity(){
  analogRead(m_inpin);
  unsigned int l_read = analogRead(m_inpin);
  double l_voltage = 5.0 * l_read / (1023.);
  return (l_voltage - m_offset)/m_slope;
}

double HumiditySensor::getHumidity(double temperature){
  if (temperature > 200) temperature -= 273.15;
  double l_sensorHum = getHumidity();
  return l_sensorHum / (1.0546-0.00216*temperature);
}

