#ifndef HUMIDITYSENSOR_h
#define HUMIDITYSENSOR_h

#include "Arduino.h"

class HumiditySensor{
public:
  HumiditySensor(int inpin, double slope, double offset);
  double getHumidity();
  double getHumidity(double temperature);
private:
  int m_inpin;
  double m_offset;
  double m_slope;

};

#endif //HUMIDITYSENSOR_h
