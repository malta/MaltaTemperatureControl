#include "NTC.h"

NTCConstants::NTCConstants(NTCType type):m_type(type) {};

//Steinhart-Hart coefficient calculator using temp-resistance data:
//https://www.thinksrs.com/downloads/programs/therm%20calc/ntccalibrator/ntccalculator.html

//SEMITEC =  Semitec 103 JT 025 
//Values from https://twiki.cern.ch/twiki/bin/view/Atlas/PixelDCSElmb#NTCs_of_IBL_and_iBBM

//TDKNTCG103JF103FTDS used on RD53A quad, coefficients from temp series on datasheet through //https://sanjit.wtf/Calibrator/webCalibrator.html

//Vishay NTCS0603E3104*XT used on Malta PCBs, coeffs from temp series on datasheet at NTCS0603E3104*XT
//https://www.eurotech.co.uk/data/NTCS0603.pdf

double NTCConstants::A(){
  switch (m_type){
  case LAIRD: return 1.370950583e-3;
  case TDK100k: return 0.7097990969e-3;
  case TDK10k: return 0.8470875090e-3;
  case TDKNTCG103JF103FTDS: return 0.8676453371787721e-3;
  case MODULE: return 1.160713496e-3;
  case MODULEPANASONIC: return 0.7712184998e-3;
  case MODULETDK: return 0.8989608015e-3;
  case MODULEMURATA: return 0.6889646222e-3;
  case SEMITEC: return 8.5824e-4;
  case VISHAY100k: return 0.6765124247e-3;
  case DUMMY: return 1;
  }
}

double NTCConstants::B(){
  switch (m_type){
  case LAIRD: return 2.421057973e-4;
  case TDK100k: return 2.185602169e-4;
  case TDK10k: return 2.612371290e-4;
  case TDKNTCG103JF103FTDS: return 2.541035850140508e-4;
  case MODULE: return 2.284638028e-4;
  case MODULEPANASONIC: return 2.710445403e-4;
  case MODULETDK: return 2.496885621e-4;
  case MODULEMURATA: return 2.893241386e-4;
  case SEMITEC: return 2.57844e-4;
  case VISHAY100k: return 2.252745404e-4;
  case DUMMY: return 1;
  }
}

double NTCConstants::C(){
  switch (m_type){
  case LAIRD: return 0.7708210696e-7;
  case TDK100k: return 0.8388329241e-7;
  case TDK10k: return 1.290719441e-7;
  case TDKNTCG103JF103FTDS: return 1.868520310774293e-7;
  case MODULE: return 1.140044319e-7;
  case MODULEPANASONIC: return 1.105639703e-7;
  case MODULETDK: return 1.988169628e-7;
  case MODULEMURATA: return 0.003558326895e-7;
  case SEMITEC: return 1.591045e-7;
  case VISHAY100k: return 0.5500296531e-7;
  case DUMMY: return 1;
  }
}

NTC::NTC(int inpin, NTCType type, double refres, bool dolpf, double lpfconstant, unsigned int smoothsteps, bool filterspikes, double spikethreshold, bool readoverntc): m_inpin(inpin), m_type(type), m_refres(refres), m_dolpf(dolpf), m_lpfconstant(lpfconstant), m_smoothsteps(smoothsteps), m_filterspikes(filterspikes), m_readoverntc(readoverntc), m_lastread(0), m_smoothedtemp(0.0), m_smoothcounter(0), m_spikethreshold(spikethreshold), m_lpftemp(0.), m_constants(0), m_smoothbuffer(0)
{
  m_constants = new NTCConstants(m_type);
  Reset();
}

NTC::~NTC(){
  delete m_smoothbuffer;
  delete m_constants;
}

void NTC::Reset(){
  //initialize smoothing. This might be a problem if the actual control cycle starts only late after the setup, but 
  //for reasonable small smoothing ranges it should be OK
  if (m_smoothbuffer != 0){
    delete m_smoothbuffer;
    m_smoothbuffer = 0;
  }
  m_smoothbuffer = new double[m_smoothsteps];
  double l_temp;
  for (unsigned int i = 0; i < m_smoothsteps; ++i){
    analogRead(m_inpin);
    m_lastread = analogRead(m_inpin);
    l_temp = getRawTemperature();
    m_smoothbuffer[i] = l_temp / m_smoothsteps;
    m_smoothedtemp += m_smoothbuffer[i];
  }
  m_previoustemps[0] = l_temp;
  m_previoustemps[1] = l_temp;
  m_lpftemp = l_temp;
  return;
}  

void NTC::Read(){
  if (m_filterspikes){
    m_previoustemps[0] = m_previoustemps[1];
    m_previoustemps[1] = getRawTemperature();
  }
  analogRead(m_inpin);
  m_lastread = analogRead(m_inpin);
  double ctemp = getRawTemperature();
  /*  if (m_filterspikes){
    double ftemp = ctemp;
    double m_newread;
    if ((fabs(m_previoustemps[1] - ftemp) > m_spikethreshold) && (fabs(m_previoustemps[0] - ftemp) > m_spikethreshold)){
      analogRead(m_inpin);
      m_newread = analogRead(m_inpin);
      ftemp = getTemperatureForRead(m_newread);
    }
    m_previoustemps[0] = m_previoustemps[1];
    m_previoustemps[1] = ctemp;
    ctemp = ftemp;
    }*/ 
  double ftemp = ctemp;
  if (m_filterspikes){
    if ((fabs(m_previoustemps[1] - ftemp) > m_spikethreshold) && (fabs(m_previoustemps[0] - ftemp) > m_spikethreshold)){
      double lowertemp, highertemp;
      if (m_previoustemps[0] > m_previoustemps[1]){
	lowertemp = m_previoustemps[1];
	highertemp = m_previoustemps[0];
      } else{
	lowertemp = m_previoustemps[0];
	highertemp = m_previoustemps[1];
      }
      if (ctemp > highertemp){
	ftemp = highertemp;
      } else {
	if (ctemp < lowertemp){
	  ftemp = lowertemp;
	}
      }
    }
  } 
  
  if (m_dolpf){
    m_lpftemp = (1-m_lpfconstant) * m_lpftemp + m_lpfconstant * ftemp;
  } else {
    m_lpftemp = ftemp;
  }
 
  if (m_smoothsteps > 1.){
    m_smoothedtemp -= m_smoothbuffer[m_smoothcounter];
    m_smoothbuffer[m_smoothcounter] = m_lpftemp / m_smoothsteps;
    m_smoothedtemp += m_smoothbuffer[m_smoothcounter];
    m_smoothcounter = (m_smoothcounter+1)%m_smoothsteps;
  } else {
    m_smoothedtemp = m_lpftemp;
  }
}

unsigned int NTC::getReadValue(){
  return m_lastread;
}

void NTC::setReference(double newRef){
  m_refres = newRef;
}

double NTC::getResistanceForRead(unsigned int read){
  double l_res = 0.;
  if (m_readoverntc){
    l_res = m_refres * (1.*read/(1023.-read));
  } else {
    l_res = m_refres * (1023./read-1.);
  }
  return l_res;
}

double NTC::getTemperatureForResistance(double resistance){
  double l_logres = log(resistance);
  double l_temp = 1/(m_constants->A()+m_constants->B()*l_logres+m_constants->C()*l_logres*l_logres*l_logres);
  return l_temp-273.15;
}

double NTC::getTemperatureForRead(unsigned int read){
  return getTemperatureForResistance(getResistanceForRead(read));
}

double NTC::getResistance(){
  return getResistanceForRead(m_lastread);
}

double NTC::getRawTemperature(){
  return getTemperatureForRead(m_lastread);
}

double NTC::getTemperature(){
  return m_smoothedtemp;
}

double NTC::getResistanceForTemperature(double temperature){
  double l_temp = temperature + 273.15;
  double x = 1/m_constants->C()*(m_constants->A()-1/l_temp);
  double y = sqrt(m_constants->B()*m_constants->B()*m_constants->B()/m_constants->C()/m_constants->C()/m_constants->C()/27+x*x/4);
  return exp(pow(y-x/2,1./3.)-pow(y+x/2,1./3.));
}

double NTC::calibrateReference(double NTCTemperature){
  const int numpoints = 20;
  int calpt[numpoints];
  for (int i = 0; i < numpoints; ++i){
    Read();
    calpt[i] = getReadValue();
  } 
  double avpt = 0.0;
  for (int j = 0; j < numpoints; ++j){
    avpt += 1.0 * calpt[j];
  }
  avpt /= numpoints;
  
  double newrefres = getResistanceForTemperature(NTCTemperature) / (1023/avpt-1);
  setReference(newrefres);
  return newrefres;
}
