#ifndef NTC_h
#define NTC_h

#include "Arduino.h"

enum NTCType {LAIRD, TDK10k, TDK100k, TDKNTCG103JF103FTDS, MODULE, MODULEPANASONIC, MODULETDK, MODULEMURATA, SEMITEC, VISHAY100k, DUMMY};

class NTCConstants{
 public:
  NTCConstants(NTCType type);
  double A();
  double B();
  double C();
 private:
  NTCType m_type;
};

class NTC{
 public:
  NTC(int inpin, NTCType tpe, double refres, bool dolpf = false, double lpfconstant = 1., unsigned int smoothsteps = 1, bool filterspikes = false, double spikethreshold = .5, bool readoverntc = false);
  ~NTC();
  void Read();
  void Reset();
  unsigned int getReadValue();
  double getResistance();
  double getTemperature();
  double getRawTemperature();
  double getResistanceForTemperature(double temperature);
  void setReference(double newRef);
  double calibrateReference(double NTCTemperature);
 private:
  int m_inpin;
  NTCType m_type;
  double m_refres;
  bool m_dolpf;
  double m_lpfconstant;
  unsigned int m_smoothsteps;
  bool m_filterspikes;
  bool m_readoverntc;
  unsigned int m_lastread;
  double m_smoothedtemp;
  unsigned int m_smoothcounter;
  double m_spikethreshold;

  double m_lpftemp;

  NTCConstants *m_constants;
  double *m_smoothbuffer;
  double m_previoustemps[2];
 
  double getResistanceForRead(unsigned int read);
  double getTemperatureForResistance(double resistance);
  double getTemperatureForRead(unsigned int read);

};



#endif //NTC_h
