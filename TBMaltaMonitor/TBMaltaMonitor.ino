#include "EnvironmentalSensor.h"
#include <Adafruit_LiquidCrystal.h>
Adafruit_LiquidCrystal lcd(0);

enum action {NOTHING = 0,PRINTTEMP, MONITOR, CALIBRATE, RESET};

//action thingtodonext = NOTHING;
action thingtodonext = MONITOR;
//action thingtodonext = CALIBRATE;
 
const int numNTC = 4; 
const bool doRef = true;

String inputString = "";         // a String to hold incoming data
boolean stringComplete = false;  // whether the string is complete

NTC* ntcs[numNTC];
EnvironmentalSensor esens(52, A2,0.029918281, 0.808163);

double reftemp = 0.0;

//calibrate NTC versus the first reference. Do nothing if no reference is present
void calibrateNTC(){
  if (!doRef) return;
  Serial.print("Calibration\n");
  Serial.print("Acquiring reference temperature\n");
  const int numpoints = 20;
  double calref[numpoints];
  for (int i = 0; i < numpoints; ++i){
    esens.Read();
    calref[i] = esens.getTemperature();
  } 
  double avref = 0.0;
  for (int j = 0; j < numpoints; ++j){
    avref += calref[j];
  }
  avref /= numpoints;
  
  for (unsigned int n = 0; n < numNTC; ++n){
    Serial.print("Calibrating Reference Resistor for NTC ");
    Serial.print(n);
    Serial.print("\n");    
    double newrefres = ntcs[n]->calibrateReference(avref);
    Serial.print("Done. Reference Resistor for NTC ");
    Serial.print(n);
    Serial.print(" has ");
    Serial.print(newrefres);
    Serial.print(" Ohm\n");
  }
  return;
}

  
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  
  ntcs[0] = new NTC(A3, SEMITEC, 50000, true, 0.2, 1, true, 0.5, true); 
  ntcs[1] = new NTC(A7, SEMITEC, 50000, true, 0.2, 1, true, 0.5, true); 
  ntcs[2] = new NTC(A11, VISHAY100k, 499000, true, 0.2, 1, true, 0.5, true); 
  ntcs[3] = new NTC(A15, VISHAY100k, 499000, true, 0.2, 1, true, 0.5, true); 

  inputString.reserve(200);

  lcd.begin(20, 4);
  lcd.setBacklight(HIGH);
  lcd.setCursor(0, 0);
  lcd.print("Temperature");
}

void setAction(String inputString){
  if (inputString == "GETTEMP"){
    thingtodonext = PRINTTEMP;
  } else if (inputString == "MONITOR") {
    thingtodonext = MONITOR;
  } else if (inputString == "CALIBRATE") {
    thingtodonext = CALIBRATE;
  } else if (inputString == "RESET") {
    thingtodonext = RESET;
  } else if (inputString == "NOTHING") {
    thingtodonext = NOTHING;
  } else {
    Serial.print("Unknown command send ");
    Serial.print(inputString);
    Serial.print("\n");
  }
}

int readNumber() {
  char inChar;
  String inString = "";
  while (inChar != ';') {
    // get the new byte:
    if (Serial.available()) {
      inChar = (char)Serial.read();
      // add it to the inputString:
      if (inChar != ';') inString += inChar;
    }
  }
  return atoi(inString.c_str());
}

void PrintStatus(){
  if (doRef){
    esens.Read();
    Serial.print(esens.getTemperature());
    Serial.print(" ");
    Serial.print(esens.getHumidity());
    Serial.print(" ");
    Serial.print(esens.getDewPoint());
    Serial.print(" ");
    lcd.setCursor(11, 1);
    lcd.print(esens.getTemperature()); 
    lcd.setCursor(19, 1);
    lcd.print("C");
    lcd.setCursor(11, 2);
    lcd.print(esens.getHumidity()); 
    lcd.setCursor(19, 2); 
    lcd.print("%");
    lcd.setCursor(11, 3);
    lcd.print(esens.getDewPoint()); 
    lcd.setCursor(19, 3); 
    lcd.print("C");
  }
  //Serial.print(" here ");
  for (unsigned int n = 0; n < numNTC; ++n){
    Serial.print(ntcs[n]->getTemperature());
    Serial.print(" ");
    lcd.setCursor(0, n+1);
    lcd.print(ntcs[n]->getTemperature(),4);
    lcd.setCursor(8, n+1);
    lcd.print("C");
  }
  Serial.print("\n");
}

void ReadNTCs(){
  for (unsigned int n = 0; n < numNTC; ++n){
    ntcs[n]->Read();
  }
}


// the loop routine runs over and over again forever:
void loop() {
  if (stringComplete) {
    setAction(inputString);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  action nextCycle;
  if (thingtodonext == MONITOR){
    nextCycle = thingtodonext;
    thingtodonext = PRINTTEMP;
  } else {
    nextCycle = NOTHING;
  }
  switch(thingtodonext){
    case NOTHING:
      break;
    case PRINTTEMP:
      ReadNTCs();
      PrintStatus();
      break;
    case CALIBRATE:
      calibrateNTC();
      break;
    case RESET:
      for (unsigned int n = 0; n < numNTC; ++n){
        ntcs[n]->Reset();
      }
      break;
  }
  thingtodonext = nextCycle;
}

/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
//    Serial.write(inChar);
    // add it to the inputString:
    if (inChar == ';') {
      stringComplete = true;
    } else {
      inputString += inChar;
    }
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
  }
}
