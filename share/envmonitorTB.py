#!/usr/bin/env python
import serial
import sys
from sys import argv
import signal
import time
from time import time
from time import sleep
import shlex

import tempplotter

numprintvals = 7

plotter = tempplotter.TempPlotter([0,1,2,3,4,5,6])

if len(argv) < 2:
  filename = "templog.dat"
else: 
  filename = argv[1]

print ("Writing log to " + filename)

outfile = open(filename, 'w')

def stopandcleanup():
	print('Stop logging, closing ' + filename)
	outfile.close()
	ser.write(b'NOTHING;')
	sleep(1)
	sys.exit(0)

def signal_handler(signal, frame):
	stopandcleanup()

signal.signal(signal.SIGINT, signal_handler)

ser = serial.Serial('/dev/ttyACM2', 9600)
print ("Booting Controller...")
sleep(1)
print ("Done")

ser.write(b'NOTHING;')
#Drain buffer
while ser.inWaiting():  # Or: while ser.inWaiting():
    ser.readline()

ser.write(b'RESET;')
ser.write(b'GETTEMP;')

sleep(.5)

initialvalues = []
print (initialvalues, numprintvals)
while len(initialvalues) != numprintvals:
	initialread = ser.readline()
	print (type(initialread), len(initialvalues))
	initialvalues =  initialread.decode().split(' \n')[0].split(' ')
	#initialvalues =  shlex.split(str(initialread), ', ')
	print ("initialvalues", initialvalues)

plotter.initPlot(initialvalues)
plotter.addTitles(["Env Temp","Env Hum","Dew Point", "NTC0 Temp","NTC1 Temp","MALTA0 Temp", "MALTA1 Temp"])
plotter.addColors(["r","b","g","darkorange","b","r","black"])

ser.write(b'MONITOR;')
sleep(.1)
print (ser.read())

starttime = time()

while(1):
  inline = ser.readline()
  values = inline.decode().split(' \n')[0].split(" ")
  if len(values) != numprintvals: 
    print ("#", inline)
    #sleep (0.1)
    continue

  outstr = str(time())+ " "  + " ".join(values)
  fstr = outstr
  #sleep (0.1)
  plotter.updatePlot(time()-starttime,values)
  #sleep (0.1)

  #outfile.write (fstr + '\n')

