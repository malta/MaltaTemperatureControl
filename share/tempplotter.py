import matplotlib
# matplotlib.use('GTKAgg')
import matplotlib.pyplot as plt
import math
from time  import sleep

class TempPlotter:
	def __init__(self,fieldstoplot,option=1):
		self.fieldstoplot = fieldstoplot
		self.numplots = len(fieldstoplot)
		self.option = option
		if self.option==3:
			self.ncols = self.option
			self.nrows = int(math.ceil(self.numplots/float(self.ncols)))
			self.fig = plt.figure(figsize=(12, self.nrows+1))
			self.ax = []
			for i in range(self.numplots):
				if i < self.ncols:
					self.ax.append(plt.subplot2grid((self.nrows, self.ncols), (i/self.ncols, i%self.ncols)))
				else:
					self.ax.append(plt.subplot2grid((self.nrows, self.ncols), (i/self.ncols, i%self.ncols),sharex=self.ax[i%self.ncols]))
				if i<self.numplots-self.ncols:
					self.ax[i].xaxis.set_visible(False)
		elif self.option==2:
			self.ncols = self.option
			self.nrows = int(math.ceil(self.numplots/float(self.ncols)))
			self.fig = plt.figure(figsize=(10, self.nrows+1))
			self.ax = []
			for i in range(self.numplots):
				# print i, i%self.nrows, i/self.nrows
				if i%self.nrows==0:
					self.ax.append(plt.subplot2grid((self.nrows, self.ncols), (i%self.nrows, i/self.nrows)))
				else:
					self.ax.append(plt.subplot2grid((self.nrows, self.ncols), (i%self.nrows, i/self.nrows),sharex=self.ax[i/self.nrows]))
				if i%self.nrows<self.nrows-1:
					self.ax[i].xaxis.set_visible(False)
		else:
			self.fig, self.ax = plt.subplots(self.numplots, sharex=True)
		self.background = self.fig.canvas.copy_from_bbox(self.ax[0].bbox)
		self.x=list()
		self.y=[]
		self.xspan = 9999999999
		self.points=[]
		for n in range(self.numplots):
			self.y.append(list())
			self.points.append(self.ax[n].plot(self.x, self.y[n])[0])
		plt.show()
		plt.draw()
		self.fig.canvas.draw()

	def initPlot(self, initialvalues):
		self.maxy = []
		self.miny = []
		for n in range(self.numplots):
			try:
				self.miny.append(.975*float(initialvalues[self.fieldstoplot[n]]))
				self.maxy.append(1.025*float(initialvalues[self.fieldstoplot[n]]))
			except Exception as e:
				print (e, initialvalues[self.fieldstoplot[n]])

	def addTitles(self,titlelist):
		if not len(titlelist) > self.numplots:
			for t in range(len(titlelist)):
				self.ax[t].set_title(titlelist[t],pad=-11)

	def addColors(self,colorlist):
		if not len(colorlist) > self.numplots:
			for c in range(len(colorlist)):	
				self.ax[c].get_lines()[0].set_color(colorlist[c])

	def setLog(self,loglist):
		if not len(loglist) > self.numplots:
			for l in range(len(loglist)):	
                            if loglist[l] > 0:
                              self.ax[l].set_yscale('log')

	def updatePlot(self, time, values):
		numPointToPlot = 2000
		self.x.append(time)
		if len(self.x) > numPointToPlot:
			self.x =  self.x[1:]
		if self.x[-1] - self.xspan > 0: 
			xmin = self.x[-1] - self.xspan
		else:
			xmin = 0
		
		if self.option==3:
			for i in range(self.option):
				self.ax[i].set_xlim(xmin, self.x[-1])
		if self.option==2:
			for i in range(self.numplots):
				if i%self.nrows==0:
					self.ax[i].set_xlim(xmin, self.x[-1])
		else:
			self.ax[0].set_xlim(xmin, self.x[-1])
		self.fig.canvas.restore_region(self.background)
		tempy=[1.0]*self.numplots
		for n in range(self.numplots):
			try:
				tempy[n] = float(values[self.fieldstoplot[n]])
				# print self.fieldstoplot[n], 
			except Exception as e:
				print (e)
			if tempy[n] > self.maxy[n]: self.maxy[n] = tempy[n]
			if tempy[n] < self.miny[n]: self.miny[n] = tempy[n]
			self.y[n].append(tempy[n]);
			if len(self.y[n]) > numPointToPlot:
				self.y[n] =  self.y[n][1:]
			self.points[n].set_data(self.x, self.y[n])
			#print(self.x.pop(0))
			#if len(self.x) > 5:
			#	self.x=self.x.pop(0)
			#	self.y[n]=self.y[n].pop(0)
			self.ax[n].set_ylim(self.miny[n]- .05 * (self.maxy[n]-self.miny[n]), self.maxy[n]+ .05 * (self.maxy[n]-self.miny[n]))
			self.ax[n].relim()
			self.ax[n].autoscale_view()
			self.fig.canvas.draw()
			self.fig.canvas.flush_events()
			self.ax[n].draw_artist(self.points[n])
			sleep(0.1)
			#if len(self.ax[n])>10:
                        #	self.ax[n]=self.ax[n].pop(0)   
                        # 	self.y[n]=self.y[n].pop(0)
			#plt.pause(1)
